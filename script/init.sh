#!/bin/bash

if [ ! -d /data/mongodb/db ]
then
    cd /data
    tar zxvf /build/pkg/init_data.tar.gz
fi

/usr/local/mongodb/bin/mongod --auth --logappend --logpath=/data/mongodb/logs/mogodb.log --dbpath=/data/mongodb/db/ --port 27017

sleep infinity
