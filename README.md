# Init

```
git clone git@code.aliyun.com:dbuild/mongodb.git
cd mongodb
prjHome=`pwd`
mongodbVer=3.4
```

# Build image

```
docker build -t andals/mongodb:${mongodbVer} ./
```

# Run container

```
docker run -d --name=mongodb-${mongodbVer} -v /data/mongodb:/data/mongodb --net=host andals/mongodb:${mongodbVer}
```

# Add user

```
/usr/local/mongodb/bin/mongo admin:123@127.0.0.1:27017/admin

> use admin 
> show collections
> db.createUser(
   {
     user: "admin",
     pwd: "123",
     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
   }
)
Successfully added user: {
    "user" : "admin",
    "roles" : [
        {
            "role" : "userAdminAnyDatabase",
            "db" : "admin"
        }
    ]
}
> show users
> db.system.users.find()

use misc
db.createUser(
   {
     user: "misc",
     pwd: "123",
     roles: [ { role: "readWrite", db: "misc" } ]
   }
)
```
