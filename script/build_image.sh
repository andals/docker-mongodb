#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

cd $installDstRoot
tar zxvf $pkgRoot/mongodb-linux-x86_64-rhel70-3.4.4.tgz
ln -s mongodb-linux-x86_64-rhel70-3.4.4 mongodb

echo -e '\nPATH=$PATH:/usr/local/mongodb/bin' >> $HOME/.bashrc
